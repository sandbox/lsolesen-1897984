Google Drive Document Input Filter
==================================

This module provides an input filter to add a document from Gogle Drive anywhere input filters are accepted. You can choose to embed the document or link to the published document on Google Drive.

Read more about Google Drive on docs.google.com.

To add a document from Google Drive to a textarea, simply add the following code to your textarea

<code> 
    [gdoc id=xxxx type=yyyy mode=zzzz] 
</code>

where id=xxxx is the document_id from Google Drive, type=yyyy is the type of document and mode=zzzz is the optional display mode. 

This module has been sponsored by <a href="http://vih.dk">Vejle Idrætshøjskole</a> and <a href="http://discimport.dk">Discimport.dk</a>.
